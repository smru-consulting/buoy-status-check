# Buoy Status Check Script #

### What is this repository for? ###

* This script utilizes an xml feed set up on the SPOT tracker account to keep track of when the last time the SPOT trackers have pinged. If it has been longer than 3 hours, then the script will send an email to desired recipients alerting of the possible missing ping.
* Version 1.0

### How do I get set up? ###

Python 3.7 or later is required.

*Required Python Packages*

* datetime 
* pandas
* pytz
* smtplib
* pandas_read_xml
* requests

To install these python packages, in your desired python environments cmd type "pip install *package name*"

In the script, you can edit these variables:
- Sender: Whose sending the email?
- Recipient: Who to send the email too?
- Subject: Subject Line
- Message: Actual message

After installing the following packages, open up the command line and navigate with cd commands to the location of CheckStatus.py
Then type "python CheckStatus.py" and the script will run!

You can schedule this script to run automatically for a certain amount of intervals using the windows scheduler application!

### Who do I talk to? ###

* This script and repository was created by Zack Nachod
* If there is any trouble with further development or any other concerns email: zachary.nachod@gmail.com