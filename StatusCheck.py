#!/usr/bin/env python
# coding: utf-8

import requests
import xml.etree.cElementTree as ET
import pandas as pd
import pandas_read_xml as pdx
import datetime
import pytz
import smtplib

# Grab xml data and write to .xml file
r = requests.get('https://api.findmespot.com/spot-main-web/consumer/rest-api/2.0/public/feed/0mWAJtRZMn5dthTCXpAG0iRosucM29MeN/message.xml', auth=('user', 'pass'))
with open('data.xml', 'w') as f:
    f.write(r.text)

# Convert .xml file into a dataframe pandas object
df = pdx.read_xml("data.xml",  ['response','feedMessageResponse','messages','message'])

# Convert datetime column to actual datetime objects
i = 0
for DT in df.dateTime:
    df.dateTime[i] = datetime.datetime.strptime(df.dateTime[i], '%Y-%m-%dT%H:%M:%S%z')
    i = i + 1

# Sort rows between the two SPOT Trackers
bigDF = pd.DataFrame()
lilDF = pd.DataFrame()
for i in range(len(df)):
    if (df.messengerName[i] == "Big Orca"):
        bigDF = bigDF.append(df.iloc[i])
    elif (df.messengerName[i] == "Lil' Orca"):
        lilDF = lilDF.append(df.iloc[i])
        
# Get current time
currentDT = datetime.datetime.now(pytz.utc)
currentDT = currentDT.astimezone(datetime.timezone(datetime.timedelta(hours=-7)))

# Get last time buoys pinged
bigPing = bigDF["dateTime"].iloc[0]
lilPing = lilDF["dateTime"].iloc[0]

bigPing = bigPing.astimezone(datetime.timezone(datetime.timedelta(hours=-7)))
lilPing = lilPing.astimezone(datetime.timezone(datetime.timedelta(hours=-7)))

# Calculate the timedelta (difference in time) between last ping and now
bigDelta = currentDT - bigPing
lilDelta = currentDT - lilPing

print("Last known Buouy ping for Big Orca: " + str(bigPing))
print("Last known Buouy ping for Lil' Orca: " + str(lilPing))
print()
print("Time since last ping for Big Orca " + str(bigDelta))
print("Time since last ping for Lil' Orca " + str(lilDelta))

# If timdelta is greater than 3 hours send email
if (bigDelta > datetime.timedelta(hours=3)):
    api_key = "AIzaSyB0Ohz0CB_2wsqA9f0Msvd7ehyILEbNcMk"
    sender = "cab@SMRUConsulting.com"
    recipient = ["zachary.nachod@gmail.com", "jw@smruconsulting.com", "jt1@smruconsulting.com"]
    
    subject = "ALERT BUOY HAS NOT PINGED IN 3 HOURS"
    
    message = "Big or small Orca may have run away."
    
    email = "Subject: {}\n\n{}".format(subject, message)
    password = "yZxyw#29*mv"
    s = smtplib.SMTP("smtp-mail.outlook.com", 587)

    s.starttls()
    s.login(sender, password)
    s.sendmail(sender, recipient, email)
    s.quit()
    print("Email Sent")
    
else:
    print("Everything is pinging normally")



